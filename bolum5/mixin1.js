var myMixin = {
  methods:{
    yeniKisiEkle(){
      if(this.isim!=""){
        this.kisiler.push(this.isim);
        this.isim="";
      }
    },
    kisiyiSil(index){
      this.kisiler.splice(index,1);
    }
  },
  computed:{
    kisiSayisi(){
      return this.kisiler.length;
    }
  },
  watch:{
    isim:function(newVal, oldVal){
      console.log("newValue",newVal);
      console.log("oldValue",oldVal);
    }
  }
}

