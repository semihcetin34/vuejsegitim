Vue.component('baslik-ve-konu', {
  props: ['title'],
  template: '<div><h3>{{title}}</h3><p><slot></slot></p></div>'
})
