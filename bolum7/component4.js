Vue.component('baslik-ve-konu-ve-footer', {
  props: ['title'],
  template: '<div><h3>{{title}}</h3><p><slot></slot></p><br><p><slot name="footer"></slot></p></div>'
})
